
This module creates an AWS WAF Web ACL along with associated rules, rule groups, and IP sets.
---

     INPUTS
---
|Name|Description|Type|Default|Required|
|----|-----------|----|-------|--------|
|web_acl_name|The name of the Web ACL to be created|string|n/a|yes|
|web_acl_metrics|A string to associate with the Web ACL metrics|string|n/a|yes|
|waf_rule_name|The name of the WAF rule to be created|string|n/a|yes|
|waf_rule_metrics|A string to associate with the WAF rule metrics|string|n/a|yes|
|waf_rule_group_name|The name of the WAF rule group to be created|string|n/a|yes|
|waf_rule_group_metrics|A string to associate with the WAF rule group metrics|string|n/a|yes|
|waf_ipset_name|The name of the IP set to be created|string|n/a|yes|
|waf_ipset_value|The IP address or CIDR range to be added to the IP set|string|n/a|yes|
|


    OUTPUTS

|Name|Description|
|----|-----------|
|ipset|The details of the IP set created|
|rule|The details of the WAF rule created|
|rule_group|The details of the WAF rule group created|
|web_acl|The details of the Web ACL created|
|



Switch to the  **setup**  directory to initialize terraform.
---