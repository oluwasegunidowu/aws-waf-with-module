module "aws_waf" {
      source                 = "../modules/waf"
      web_acl_name           = "MyFirstWebacl"
      web_acl_metrics        = "MyFirstWebaclmetrics"
      waf_rule_name          = "MyfirstWafRuleName"
      waf_rule_metrics       = "MyfirstWafRuleMetrics"
      waf_rule_group_name    = "MyfirstWafRuleGroupName"
      waf_rule_group_metrics = "MyfirstWafRuleGroupMetrics"
      waf_ipset_name         = "MyIPSet"
      waf_ipset_value        = "10.0.0.0/24"
}

output "ipset" {
    value = module.aws_waf.ipset_details
}

output "rule" {
    value = module.aws_waf.waf_rule
}

output "rule_group" {
    value = module.aws_waf.waf_rule_group
}

output "web_acl" {
    value = module.aws_waf.waf_web_acl
}
