output "ipset_details"{
    value        = aws_waf_ipset.wafipset
}

output "waf_rule"{
    value        = aws_waf_rule.wafrule
}

output "waf_rule_group"{
    value       = aws_waf_rule_group.rule_group
}

output "waf_web_acl"{
    value       = aws_waf_web_acl.waf_acl
}