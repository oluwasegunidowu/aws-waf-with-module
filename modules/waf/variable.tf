variable "waf_ipset_name" {
  type        = string
}

variable "waf_ipset_value" {
  type        = string
}

variable "waf_rule_name" {
  type        = string
}

variable "waf_rule_metrics" {
  type        = string
}

variable "waf_rule_group_name" {
  type        = string
}

variable "web_acl_name" {
  type        = string
}

variable "web_acl_metrics" {
  type        = string
}

variable "waf_rule_group_metrics" {
  type        = string
}

