Resources Created
---
This module creates four AWS resources:

1. An AWS WAF IP set
2. An AWS WAF rule
3. An AWS WAF rule group
4. An AWS WAF web ACL

Resources
---
1. aws_waf_ipset
This resource creates an AWS WAF IP set and adds a single IP address or CIDR range to it.

2. aws_waf_rule
This resource creates an AWS WAF rule that uses the IP set created by the aws_waf_ipset resource to block traffic to the associated AWS Web ACL.

3. aws_waf_rule_group
This resource creates an AWS WAF rule group that includes the rule created by the aws_waf_rule resource.

4. aws_waf_web_acl
This resource creates an AWS WAF Web ACL with a single rule that blocks traffic based on the WAF rule group created by the aws_waf_rule_group resource.